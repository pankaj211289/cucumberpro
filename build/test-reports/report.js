$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("generate-prime-number.feature");
formatter.feature({
  "line": 3,
  "name": "Generate Prime Numbers",
  "description": "",
  "id": "generate-prime-numbers",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@all"
    },
    {
      "line": 2,
      "name": "@prime-numbers"
    }
  ]
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "initialize class to generate prime numbers",
  "keyword": "Given "
});
formatter.match({
  "location": "PrimeNumberSteps.initializePrimeNumberFactory()"
});
formatter.result({
  "duration": 171600124,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Print and verify prime numbers",
  "description": "",
  "id": "generate-prime-numbers;print-and-verify-prime-numbers",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@print-prime-numbers"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "print first 10 prime numbers",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "verify that prime number at position 9 is 29",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "verify that prime number at position 0 is 2",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 12
    }
  ],
  "location": "PrimeNumberSteps.printPrimeNumber(int)"
});
formatter.result({
  "duration": 1442893,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "9",
      "offset": 37
    },
    {
      "val": "29",
      "offset": 42
    }
  ],
  "location": "PrimeNumberSteps.VerifyNumberAtPosition(int,int)"
});
formatter.result({
  "duration": 113198,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 37
    },
    {
      "val": "2",
      "offset": 42
    }
  ],
  "location": "PrimeNumberSteps.VerifyNumberAtPosition(int,int)"
});
formatter.result({
  "duration": 84823,
  "status": "passed"
});
});