package cucumberproject.stepdefs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberproject.pageobjects.AccountPage;

public class HomeScreenStepDefs {

	private AccountPage accountPage = new AccountPage();
	
	@Then("^I can see home screen$")
	public void verifyHomeScreen() throws Exception {
		accountPage.verifyHomeScreenPage();
	}
	
	@When("^I click on user profile and logout$") 
	public void logout() {
		accountPage.logout();
	}
}
