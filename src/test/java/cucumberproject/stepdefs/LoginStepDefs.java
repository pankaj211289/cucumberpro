package cucumberproject.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberproject.factory.Driver;
import cucumberproject.factory.UserInfo;
import cucumberproject.pageobjects.LoginPage;

public class LoginStepDefs {
	
	private LoginPage loginPage = new LoginPage();

	@Given("^navigate to sign in page$")
	public void navigateToLoginPage() {
		Driver.getInstance();
	}
	
	@Then("^I can see sign in page$")
	public void verifyLoginPage() {
		loginPage.verifySignUpForm();
	}
	
	@When("^I fill up username \"([^\"]*)\"$")
	public void fillUserName(String userName) {
		loginPage.enterUserName(UserInfo.USER1.getUserName());
	}
	
	@When("^I fill up password of user \"([^\"]*)\"$")
	public void fillPassword(String userName) {
		loginPage.enterPassword(UserInfo.USER1.getPassword());
	}
}

