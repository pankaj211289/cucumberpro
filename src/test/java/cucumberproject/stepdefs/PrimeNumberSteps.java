package cucumberproject.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberproject.PrimeNumber;
import cucumberproject.impl.GeneratePrimeNumber;

public class PrimeNumberSteps {

	private PrimeNumber primeNumber; 
	
	@Given("^initialize class to generate prime numbers$") 
	public void initializePrimeNumberFactory() {
		primeNumber = new GeneratePrimeNumber();
	}
	
	@When("^print first (\\d+) prime numbers$") 
	public void printPrimeNumber(int count) {
		primeNumber.printFirst(count);
	}
	
	@Then("^verify that prime number at position (\\d+) is (\\d+)$")
	public void VerifyNumberAtPosition(int pos, int num) {
		primeNumber.numberAtPosition(pos, num);
	}
}
