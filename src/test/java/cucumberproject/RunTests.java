package cucumberproject;

import org.junit.Before;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:cucumber-html-reports", "json:cucumber-html-reports/cucumber.json"},
        features = "src/test/resources/",
        glue = "cucumberproject/stepdefs",
        tags = {"@user-login"}
)
public class RunTests {

    @Before
    public static void setUp() {
    }
}
