@all
@prime-numbers
Feature: Generate Prime Numbers

	Background: 
		Given initialize class to generate prime numbers
	
	@print-prime-numbers
	Scenario: Print and verify prime numbers
		When print first 10 prime numbers
		Then verify that prime number at position 9 is 29
		And verify that prime number at position 0 is 2