@all
@user-login
Feature: User Login Functionality

	Background: 
		Given navigate to sign in page
	
	@userlogin
	Scenario: Verify User Login
		When I fill up username "demobtes@gmail.com"
		And I fill up password of user "demobtes@gmail.com"
		Then I can see home screen
		
	@userLogout
	Scenario: Verify User Login	
		When I click on user profile and logout
		Then I can see sign in page