package cucumberproject.pageobjects;

import org.openqa.selenium.By;

public class AccountPage {

	private By accountContentBy = By.cssSelector("[data-module='page-amsterdam']");
	
	private By userLogoBy = By.cssSelector(".user-default-avatar");
	private By logoutBy = By.cssSelector("[data-resin-target='logout']");
	
	
	public void logout() {
		BasicActions.click(userLogoBy);
		BasicActions.click(logoutBy);
	}
	
	public void verifyHomeScreenPage() throws Exception {
		if(BasicActions.findElement(accountContentBy) == null)
			throw new Exception();
	}
}
