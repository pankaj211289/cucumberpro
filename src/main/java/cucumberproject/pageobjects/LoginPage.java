package cucumberproject.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import cucumberproject.factory.Driver;

public class LoginPage {

	private By usernameBy = By.name("login");
	private By passwordBy = By.name("password");
	private By nextBtnBy = By.cssSelector("[type='submit']");
	
	private By signUpFormBy = By.cssSelector(".login-container");
	
	public void enterUserName(String userName) {
		WebElement username = BasicActions.findElement(usernameBy);
		BasicActions.enterText(username, userName);
		BasicActions.click(nextBtnBy);
		BasicActions.waitToStale(username);
	}
	
	public void enterPassword(String password) {
		WebElement passwordEle = BasicActions.findElement(passwordBy);
		BasicActions.enterText(passwordEle, password);
		BasicActions.click(nextBtnBy);
		BasicActions.waitToStale(passwordEle);
	}
	
	public void verifySignUpForm() {
		if(BasicActions.findElement(signUpFormBy) == null)
			throw new IllegalStateException();
		Driver.closeDriver();
	}
}
