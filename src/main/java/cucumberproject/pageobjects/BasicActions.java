package cucumberproject.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberproject.factory.Driver;

public class BasicActions {

	private static WebDriver driver = Driver.getInstance(); 
	
	public static void enterText(By by, String text) {
		findElement(by).sendKeys(text);
	}
	
	public static void enterText(WebElement we, String text) {
		we.sendKeys(text);
	}
	
	public static void click(By by) {
		findElement(by).click();
	}
	
	public static WebElement findElement(By locator) {
		WebDriverWait driverWait = new WebDriverWait(driver, 40);
		return driverWait.until(ExpectedConditions.elementToBeClickable(locator));
	}
	
	public static void waitToStale(WebElement we) {
		WebDriverWait driverWait = new WebDriverWait(driver, 40);
		driverWait.until(ExpectedConditions.stalenessOf(we));
	}
}
