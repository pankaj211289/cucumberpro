package cucumberproject.impl;

import java.util.ArrayList;
import java.util.List;

import cucumberproject.PrimeNumber;

public class GeneratePrimeNumber implements PrimeNumber {

	private List<Integer> primeNumberList;
	
	@Override
	public void printFirst(int num) {
		primeNumberList = new ArrayList<>();
		
		int i = 0;
		int number = 2;
		boolean isPrime = true;
		
		while(i < num) {
			for(int j = 2; j < number; j++) {
				if(number % j == 0) {
					isPrime = false;
					break;
				}
			}
			if(isPrime) {
				primeNumberList.add(number);
				i++;
			}
			
			isPrime = true;
			number++;
		}
		
		System.out.println("Printing first " + num + " prime numbers");
		for(int primeNum : primeNumberList) {
			System.out.print(primeNum + " ");
		}
		System.out.println("---------------------------------------" + "\n");
	}

	@Override
	public void numberAtPosition(int pos, int num) {
		if(primeNumberList != null) {
			System.out.println("Prime number at position " + pos + " is " + num);
		}
	}
}
