package cucumberproject;

public interface PrimeNumber {

	public void printFirst(int num);
	
	public void numberAtPosition(int pos, int num);
}
