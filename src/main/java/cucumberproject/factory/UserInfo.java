package cucumberproject.factory;

public enum UserInfo {

	USER1("pdevi@bebotechnologies.com", "test123");

	private String username;
	private String password;
	
	UserInfo(String userName, String password) {
		this.username = userName;
		this.password = password;
	}
	
	public String getUserName() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}
}
