package cucumberproject.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {

	private static WebDriver instance = null;
	private static String loginURL = "https://account.box.com/login";
	
	public static WebDriver getInstance() {
		System.setProperty("webdriver.chrome.driver","chromedriver.exe"); 
		
		if(instance == null) {
			instance = new ChromeDriver();
		}
		
		instance.get(loginURL);
		return instance;
	}
	
	public static void closeDriver() {
		if(instance != null) {
			instance.quit();
			instance = null;
		}
	}
}
