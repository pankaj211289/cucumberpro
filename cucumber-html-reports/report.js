$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("user-login.feature");
formatter.feature({
  "line": 3,
  "name": "User Login Functionality",
  "description": "",
  "id": "user-login-functionality",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@all"
    },
    {
      "line": 2,
      "name": "@user-login"
    }
  ]
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "navigate to sign in page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStepDefs.navigateToLoginPage()"
});
formatter.result({
  "duration": 7164150207,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Verify User Login",
  "description": "",
  "id": "user-login-functionality;verify-user-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@userlogin"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "I fill up username \"demobtes@gmail.com\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I fill up password of user \"demobtes@gmail.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I can see home screen",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "demobtes@gmail.com",
      "offset": 20
    }
  ],
  "location": "LoginStepDefs.fillUserName(String)"
});
formatter.result({
  "duration": 1665688337,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "demobtes@gmail.com",
      "offset": 28
    }
  ],
  "location": "LoginStepDefs.fillPassword(String)"
});
formatter.result({
  "duration": 5635088742,
  "status": "passed"
});
formatter.match({
  "location": "HomeScreenStepDefs.verifyHomeScreen()"
});
formatter.result({
  "duration": 39331210,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "navigate to sign in page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStepDefs.navigateToLoginPage()"
});
formatter.result({
  "duration": 2530104996,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Verify User Login",
  "description": "",
  "id": "user-login-functionality;verify-user-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 14,
      "name": "@userLogout"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "I click on user profile and logout",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "I can see sign in page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeScreenStepDefs.logout()"
});
formatter.result({
  "duration": 1972158202,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefs.verifyLoginPage()"
});
formatter.result({
  "duration": 663861779,
  "status": "passed"
});
});